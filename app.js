// require( './public/js/db' );
var express = require('express');
var upload = require('jquery-file-upload-middleware');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var jquery = require('jquery');
var passport = require('passport');
var flash    = require('connect-flash');
var morgan = require('morgan');
var session = require('express-session');
var configDB = require('./public/js/config/database.js');
var mongoose = require('mongoose');
var mime = require('mime');
var spawn = require('child_process').spawn;
var socket_io    = require( "socket.io" );
// var multer  = require('multer');
// var upload = multer({ dest: 'uploads/' });




mongoose.connect('mongodb://localhost/BridgeLabel');

require('./public/js/config/passport.js')(passport);


var routes = require('./routes/index');
var users = require('./routes/users');
var test = require('./routes/test');

//console.log("TestThis1");
var app = express();

var io = socket_io();
app.io = io;



// view engine setup
// app.engine('ejs', require('ejs').renderFile);
app.engine('html', require('ejs').renderFile);
app.set('views', path.join(__dirname, 'public/views'));
// app.set('view engine', 'ejs');
app.set('view engine', 'html');
//app.set('view engine', 'jade');
// add this before app.use( express.json());
// app.use( express.bodyParser());
// add this line to the routes section
  

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(morgan('dev'));
app.use(bodyParser.json());
 app.use(bodyParser.urlencoded());
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(__dirname + '/public'));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));
//app.use('/upload', upload.fileHandler());

app.use(session({ secret: 'bridgelabeler' }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash()); //



// app.get('/result', function(req, res) { 
//     res.setHeader('Content-disposition', 'attachment; filename=features.mat');
//     mimetype = mime.lookup('features.mat');
//     res.setHeader('Content-type', mimetype);
//     res.sendFile('features.mat');
// })

// app.get('/matlabpost', function(req, res, next) {
//       res.set('content-type', 'text/html');
//       m = spawn('matlab', ['-r', "cd matlab; s = server(host, port); s.receive(); s.send('This is MatLab'); exit;"]);
//       m.stderr.on('data', function(d) { 
//           console.log(d.toString('utf-8'));
//           res.write('<div style="color: red">' + d + '</div>', 'utf-8');
//       })
//       m.stdout.on('data', function(d) { 
//           console.log(d.toString('utf-8'));
//           res.write('<div>' + d + '</div>', 'utf-8');
//       })
//       m.on('close', function() {
//           res.write('<a href="result">Click to download the result</a>');
//           res.end();
//       })
//   });


  
  require('./routes/index.js')(app, passport, upload, __dirname, io);
 


 


// var passScript = require('./routes/index.js')(app, passport);
// app.use(require('./routes/index.js')(app, passport)); //


// upload.configure({
//       imageVersions: {
//           thumbnail: {
//               width: 80,
//               height: 80
//           }
//       }
//   });

// upload.on('end', function (fileInfo, req, res) {
//     console.log("Uploaded file.");
//  });

// /// Redirect all to home except post
// app.get('/upload', function( req, res ){
//     res.redirect('/');
// });

// app.put('/upload', function( req, res ){
//     res.redirect('/');
// });

// app.delete('/upload', function( req, res ){
//     res.redirect('/');
// });

// app.use('/upload',  function(req, res, next){
//     upload.fileHandler({
//         uploadDir: function () {

//             return __dirname + '/public/uploads/'
//         },
//         uploadUrl: function () {
//             return '/uploads'
//         }
//     })(req, res, next);
//     // console.log(__dirname + '/public/uploads/'+req.user.username);
// });
// app.get('/jquery/jquery.js', function(req, res) {
//     res.sendFile(__dirname + '/node_modules/jquery/dist/jquery.min.js');
// });
// app.use('/', routes);
// // app.use('/login', routes);
// // app.use('/signup', routes);
// app.use('/users', users);
// app.use('/test', test);

//app.post( '/create', routes.create );

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
