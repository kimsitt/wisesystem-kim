var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var BridgeLabel = new Schema({
    user_id    : String,
    content    : String,
    updated_at : Date
});

mongoose.model( 'BridgeLabel', BridgeLabel );
mongoose.connect( 'mongodb://localhost/BridgeLabel' );