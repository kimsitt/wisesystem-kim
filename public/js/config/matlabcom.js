net = require('net');
var glob_io;

function Client(host, port, io){
    glob_io = io;
    this.queue = [];
    this.socket = new net.Socket();
    this.socket.connect(port, host, function() {
        console.log('Connected');
    });
    this.queue = [];
}

Client.prototype.send = function (data){
    this.socket.write(data+'\n');
}

Client.prototype.receive = function (){
    var that = this;
    glob_io.on('connection', function (socket) {
        that.socket.on('data', function(data) {
            that.queue.push(data); 
            var recMessage = ''+data;
            console.log(recMessage);
            socket.emit('news', { data: recMessage });
        });
        socket.on('my other event', function (data) {
            console.log(data);
        });
    });
    
}

Client.prototype.disconnect = function (){
    this.socket.on('close', function() {
        console.log('Connection closed');
        this.socket.destroy();
    });
}

module.exports = Client;