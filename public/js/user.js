// app/models/user.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var passportLocalMongoose = require('passport-local-mongoose');

// define the schema for our user model
var userSchema = new mongoose.Schema({

    username    	 : String,
    password    	 : String,
    
});

var fileScheme = new mongoose.Schema({

    username    	 : String,
 	filename         : String,
    emolist     	 :     [],
    apnlist     	 :     [],
    pnnlist      	 :     [],
    regionstartlist  :     [],
    regionendlist    :     [],
   

});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

userSchema.plugin(passportLocalMongoose);

var users = mongoose.model('User', userSchema, 'users');
var filelist = mongoose.model('File', fileScheme, 'files');
// create the model for users and expose it to our app
module.exports = {
   User:users, 
   File:filelist,
};