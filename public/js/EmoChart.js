var c = document.getElementById("EmoChart");
var ctx = c.getContext("2d");

var Eheight = c.height;
var Ewidth = c.width;
console.log(Eheight + " " + Ewidth);

ctx.moveTo(Eheight / 2, 10);
ctx.lineTo(Eheight / 2, Ewidth - 10);
ctx.stroke();

ctx.moveTo(10, Ewidth / 2);
ctx.lineTo(Eheight - 10, Ewidth / 2);
ctx.stroke();



// Create neutral gradient
var radius = 20;
var NeutralPosx = Ewidth / 2;
var NeutralPosy = Eheight / 2;

var grdNeutral = ctx.createRadialGradient(NeutralPosx, NeutralPosy, 10, NeutralPosx+20, NeutralPosy+20, 40);
grdNeutral.addColorStop(0, "#00b3b3");
grdNeutral.addColorStop(1, "gray");

ctx.beginPath();
ctx.arc(NeutralPosx, NeutralPosy, radius, 0, 2 * Math.PI, false);
ctx.fillStyle = grdNeutral;
ctx.fill();
ctx.lineWidth = 10;
ctx.strokeStyle = grdNeutral;
ctx.stroke();

// Create Circle
ctx.beginPath();
ctx.arc(NeutralPosx,NeutralPosy,5*Ewidth/16,0,2*Math.PI);
ctx.lineWidth = 2;
ctx.strokeStyle = "black";
ctx.stroke();

// Create anger gradient
var radius = 20;
var AngerPosx = 4*Ewidth / 16 + 15;
var AngerPosy = 4*Eheight / 16 + 15;

var grdAnger = ctx.createRadialGradient(AngerPosx, AngerPosy, 10, AngerPosx+20, AngerPosy+20, 40);
grdAnger.addColorStop(0, "#e65c00");
grdAnger.addColorStop(1, "red");

ctx.beginPath();
ctx.arc(AngerPosx, AngerPosy, radius, 0, 2 * Math.PI, false);
ctx.fillStyle = grdAnger;
ctx.fill();
ctx.lineWidth = 10;
ctx.strokeStyle = grdAnger;
ctx.stroke();


// Create happy gradient
var radius = 20;
var HappyPosx = 12 *Ewidth / 16 - 15;
var HappyPosy = 4*Eheight / 16 + 15;

var grdHappy = ctx.createRadialGradient(HappyPosx, HappyPosy, 10, HappyPosx+20, HappyPosy+20, 40);
grdHappy.addColorStop(0, "#e65c00");
grdHappy.addColorStop(1, "#e6e600");

ctx.beginPath();
ctx.arc(HappyPosx, HappyPosy, radius, 0, 2 * Math.PI, false);
ctx.fillStyle = grdHappy;
ctx.fill();
ctx.lineWidth = 10;
ctx.strokeStyle = grdHappy;
ctx.stroke();


// Create sad gradient
var radius = 20;
var SadPosx = 4*Ewidth / 16 + 15;
var SadPosy = 12 *Ewidth / 16 - 15;

var grdSad = ctx.createRadialGradient(SadPosx, SadPosy, 10, SadPosx+20, SadPosy+20, 40);
grdSad.addColorStop(0, "#00b3b3");
grdSad.addColorStop(1, "blue");

ctx.beginPath();
ctx.arc(SadPosx, SadPosy, radius, 0, 2 * Math.PI, false);
ctx.fillStyle = grdSad;
ctx.fill();
ctx.lineWidth = 10;
ctx.strokeStyle = grdSad;
ctx.stroke();

ctx.font = "16px bold Georgia";
ctx.textShadow = "0px 0px 4px #3d768a";
ctx.textDecoration = "none";
ctx.fillStyle = 'white';

ctx.fillText("Active", Ewidth / 2 + 5, 20);
ctx.fillText("Passive", Ewidth / 2 + 5, Eheight - 20);
ctx.fillText("Negative", 10, Eheight / 2 + 20);
ctx.fillText("Positive", Ewidth - 60, Eheight / 2 + 20);


ctx.font = "12px bold Georgia";
ctx.fillText('Neutral', NeutralPosx-20, NeutralPosy+5);
ctx.fillText('Anger', AngerPosx-18, AngerPosy+5);
ctx.fillText('Happy', HappyPosx-18, HappyPosy+5);
ctx.fillText('Sad', SadPosx-15, SadPosy+5);
