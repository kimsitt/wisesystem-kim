$(document).ready(function() {
	// var ipAd = "system.wise.audio";
	var ipAd = "localhost:3000";
	var username = $('#User').text();
	var fileName = "";
	var regionList = new Array();
	var primaryD = 1;
	var secondaryD = 10;
	var suggestemo;

	

	$('#fileupload').fileupload({ dataType: 'file' });
	
	$('#fileupload').bind('fileuploadadd', function (e, data) {
	  $.each(data.files, function (index, file) {
	    fileName = file.name;
	  });
	});

	$('#fileupload').bind('fileuploadsubmit', function (e, data) {
	  data.formData = {"file" : fileName};
	});


	
	/*var chartEmo;*/
	var wavesurfer;
	var zoomLevel = 20;
	window.onload = function() {
	

		$("#acceptsuggest").css("visibility", "hidden");
		$("#suggest").css("visibility", "hidden");
		$("#removefrommodel").css("display", "none");
	
		document.getElementById("suggest").style.display = "none";
		var socket = io.connect('http://'+ipAd);
		
		socket.emit("join", username);
        
        socket.on('console', function (data) {
	        document.getElementById("suggest").style.display = "inherit";
	        if (data !== "-"){
	        	$('#createmodel').show();
	        	$('#addtraining').show();
	        	$('#requestlabel').show();
	        	$("#inspectmodel").show();
	        	$('#console').prepend( "<p>" + data + "</p>" );
	        }

        });

        socket.on('fileremoved', function (data) {
        	var modelname = $('#modellist').val();
        	$('#console').prepend( "<p>" + data + "</p>" );
        	socket.emit("inspectmodel", username, modelname);
        });

        socket.on('modellist', function (data) {
         	
         	$('#modellisttable').empty();
         	// $('#modellisttable').append( "<form id=\"removeform\"action=\"http://"+ipAd+"/wise/send/removeindex\" method=\"post\"> </form>" );
	      
	        for(var i=0; i<data.length;i++){
	        	
		        $('#modellisttable').append( "<input type=\"checkbox\" name=\"index\" value=\""+ i + "\" style=\'font-size: 20px\'> " + data[i] + "<br>" );

	        }

	        // $('#removeform').append( "<br><input id=\"removefrommodel\" class=\"btn btn-danger\"type=\"submit\" value=\"Remove selected files\">" );
	        // $('#modellisttable').append( "</form>" );

        });

        socket.on("disconnect", function(){
           $('#console').prepend( "<p>" + "Disconnected from server..." + "</p>" );
        });

        socket.on('suggestion', function (emo) {
        	if (emo !== null) {
		        document.getElementById("suggest").style.display = "inherit";
		        $('#recomsec').text('Suggested labels: ' + emo);
		        $("#acceptsuggest").css("visibility", "visible");  
		        $("#suggest").css("visibility", "visible");
		        suggestemo = emo;
		    }
	          // console.log(data);
        });

        $("#inspectmodel").click(function(){
        	if ($("#mainwin").css('display') == 'block') {
	        	$("#mainwin").css("display", "none");
	        	$("#addtraining").css("display", "none");
	        	$("#createmodel").css("display", "none");
	        	$("#requestlabel").css("display", "none");
	        	$("#modelwin").css("display", "block");
	        	$("#removefrommodel").css("display", "block");
	        	$("#inspectmodel").html("<p class=\"fa fa-eye-slash fa-fw\"></p> Main menu");
	        }
	        else {
	        	$("#mainwin").css("display", "block");
	        	$("#addtraining").css("display", "block");
	        	$("#createmodel").css("display", "block");
	        	$("#requestlabel").css("display", "block");
	        	$("#modelwin").css("display", "none");
	        	$("#removefrommodel").css("display", "none");
	        	$("#inspectmodel").html("<p class=\"fa fa-eye fa-fw\"></p> Inspect model");

	        }
	        var modelname = $('#modellist').val();
	        socket.emit("inspectmodel", username, modelname);
        	// $(".sidenav").width($(window).innerWidth());
        });

        $('#modellist').on('change', function() {
			modelname=this.value;
			socket.emit("inspectmodel", username, modelname);
		});

		$("#acceptsuggest").click(function() {
			var emo = suggestemo.split(", ");

			if (lastRegion !== undefined && emo.length === 3) {
				lastRegion.emoLabel = emo[0];
				lastRegion.apnLabel = emo[1];
				lastRegion.pnnLabel = emo[2];
				if (emo[0] == "Anger"){
					lastRegion.region.update({
						color: 'rgba(255, 0, 0, 0.3)'
					});
				} else if (emo[0] == "Disgust"){
					lastRegion.region.update({
						color: 'rgba(0, 255, 0, 0.3)'
					});
				} else if (emo[0] == "Fear") {
					lastRegion.region.update({
						color: 'rgba(153, 50, 255, 0.3)'
					});
				}else if (emo[0] == "Happy") {
					lastRegion.region.update({
						color: 'rgba(255, 150, 50, 0.3)'
					});
				}else if (emo[0] == "Neutral") {
					lastRegion.region.update({
						color: 'rgba(255, 255, 255, 0.3)'
					});
				}else if (emo[0] == "Sad") {
					lastRegion.region.update({
						color: 'rgba(0, 0, 255, 0.5)'
					});
				}
				refreshTable();
				savelabels();

				var index = findRegion(lastRegion.region);
				var emoval;
				var apnval;
				var pnnval;
				var regionstartval;
				var regionendval;

				var reqstatus = 'add';

				emoval = regionList[index].emoLabel;
				apnval = regionList[index].apnLabel;
				pnnval = regionList[index].pnnLabel;
				regionstartval = regionList[index].region.start;
				regionendval = regionList[index].region.end;
				var fileN = $('#filelist').val();
				var modelname = $('#modellist').val();

				socket.emit("addtoTraining", username, modelname, fileN, emoval, apnval, pnnval, regionstartval, regionendval);

			}
			$("#acceptsuggest").css("visibility", "hidden");
			$("#suggest").css("visibility", "hidden");
		});

        $('#addtraining').click(function() {
		
			if (lastRegion !== undefined) {
				if (!lastRegion.emoLabel.trim())
					alert('Operation failed: Emotion label is missing!')
				else if (!lastRegion.apnLabel.trim())
					alert('Operation failed: Arousal label is missing!')
				else if (!lastRegion.pnnLabel.trim())
					alert('Operation failed: Valance label is missing!')
				else{
					// addTrainingData();
					var index = findRegion(lastRegion.region);
					var emoval;
					var apnval;
					var pnnval;
					var regionstartval;
					var regionendval;

					var reqstatus = 'add';

					emoval = regionList[index].emoLabel;
					apnval = regionList[index].apnLabel;
					pnnval = regionList[index].pnnLabel;
					regionstartval = regionList[index].region.start;
					regionendval = regionList[index].region.end;
					var fileN = $('#filelist').val();
					var modelname = $('#modellist').val();

					socket.emit("addtoTraining", username, modelname, fileN, emoval, apnval, pnnval, regionstartval, regionendval);
				}

			}
			else
			{
				alert('Operation failed: Please select a segment!')
			}
		
		});

		$('#removefrommodel').click(function() {
			var indexlist = new Array();
			var modelname = $('#modellist').val();
			$('#modellisttable').children('input').each(function () {
			    indexlist.push(this.checked); // "this" is the current element in the loop
			});
			socket.emit("removefromTraining", username, modelname, indexlist);
		});

		$('#requestlabel').click(function() {
		
			if (lastRegion !== undefined) {
				var index = findRegion(lastRegion.region);
				var emoval;
				var apnval;
				var pnnval;
				var regionstartval;
				var regionendval;

				emoval = regionList[index].emoLabel;
				apnval = regionList[index].apnLabel;
				pnnval = regionList[index].pnnLabel;
				regionstartval = regionList[index].region.start;
				regionendval = regionList[index].region.end;
				var fileN = $('#filelist').val();
				var modelname = $('#modellist').val();

				socket.emit("requestlabel",username, modelname, fileN, regionstartval, regionendval);
				// requestlabels();
			}
			else
			{
				alert('Operation failed: Please select a segment!')
			}
		
		});

		$('#discardsuggest').click(function() {
			$("#acceptsuggest").css("visibility", "hidden");
			$("#suggest").css("visibility", "hidden");
		});

		$('#createmodel').click(function() {

			var modelname = $('#modellist').val();
			if (modelname === null) {
				alert('Please select a model!');
			}
			else {
				$('#createmodel').hide();
	        	$('#addtraining').hide();
	        	$('#requestlabel').hide();
	        	$("#inspectmodel").hide();
				$('#console').prepend( "<p> Please wait while the model is updating... </p>" );
				socket.emit("createmodel", username, modelname);
			}
				
		
		});

		$("#addmodel").click(function() {
		
			var modelname=prompt("Please enter model name:");

			$('#modellist').append($('<option>', {
						    value: modelname,
						    text: modelname
						}));

			$('#modellist').val(modelname);
			    
			socket.emit("addmodel", username, modelname);
				
		
		});

		$("#removemodel").click(function() {
		
			var modelname = $('#modellist').val();

			$('#modellist').find('option:selected').remove();
			socket.emit("removemodel", username, modelname);
				
		
		});

		/******************** Wavesurfer ******************************/

		wavesurfer = Object.create(WaveSurfer);
		wavesurfer.init({
			container: '#wave',
			waveColor: 'black',
			progressColor: '#ff9933',
			minPxPerSec: zoomLevel,
			scrollParent: true
		});

		



		function getmodellist() {
			var results;
			$.ajax({
			    url: "http://"+ipAd+"/labeling/"+username+"/model/list",
			    type: "GET",
			    crossDomain: true,
			    // contentType: 'application/json',  
			    data:  {results: results},
			    success: function(data){
			    	$('#modellist').empty();
			    	for (var i = 0; i < data.results.length; i++) {
			    		var str = data.results[i].split(".");
			    		var strtemp = str[0].substring(str[0].length-6,str[0].length)
	        			if (strtemp !== "minmax"){
	        				if (str === "LDC") {
					    		$('#modellist').append($('<option>', {
								    value: data.results[i],
								    text: data.results[i]
								}));
					    	}
					    	else {
					    		$('#modellist').prepend($('<option>', {
								    value: data.results[i],
								    text: data.results[i]
								}));
					    	}
						}		    		    		
			    	}
			    	$("#modellist").prop('selectedIndex', 0);		
	            }
			});
			
		}

		getmodellist();

		function getfilelist() {
			var results;
			$.ajax({
			    url: "http://"+ipAd+"/labeling/"+username+"/filelist",
			    type: "GET",
			    crossDomain: true,
			    contentType: 'json',  
			    data:  {results: results},
			    success: function(data){
			    	$('#filelist').empty();
			    	for (var i = 0; i < data.results.length; i++) {
			    		$('#filelist').append($('<option>', {
						    value: data.results[i],
						    text: data.results[i]
						}));		    		    		
			    	}	
	            },
	            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    alert("Status: " + textStatus); alert("Error: " + errorThrown); 
                }

			});
			
		}

		function initfilelist() {
			var results;
			$.ajax({
			    url: "http://"+ipAd+"/labeling/"+username+"/filelist",
			    type: "GET",
			    crossDomain: true,
			    // contentType: 'application/json',  
			    data:  {results: results},
			    success: function(data){
			    	for(var i = regionList.length-1; i >= 0; i--)
					{
						regionList[i].remove();
						regionList.splice(i, 1);
						lastRegion = undefined;
						currentIndex = -1;
					}
			    	$('#filelist')
					    .find('option')
					    .remove()
					    .end()
					;
					refreshTable();
					wavesurfer.empty();
			    	for (var i = 0; i < data.results.length; i++) {
			    		$('#filelist').append($('<option>', {
						    value: data.results[i],
						    text: data.results[i]
						}));
			    		if(i === 0) {
			    			$('#filelist').val(data.results[i]);
			    			wavesurfer.load('../uploads/'+username+ '/' + data.results[i]);
							wavesurfer.on('ready', function() {
								//wavesurfer.play();
								// Enable creating regions by dragging
								wavesurfer.enableDragSelection({
									color: 'rgba(102,255,51, 0.2)'
								});
								var timeline = Object.create(WaveSurfer.Timeline);

								timeline.init({
									wavesurfer: wavesurfer,
									container: "#wave-timeline", 
									primaryColor: 'red',
									primaryFontColor: 'red',
									secondaryFontColor: 'red',
									notchPercentHeight: '90',
									height: '20',
									primaryLabelInterval: primaryD,
									secondaryLabelInterval: secondaryD
								});
								var fileN= ''+data.results[i];
								var e = document.getElementById("filelist");
								if (e.selectedIndex > -1) {
									var strUser = e.options[e.selectedIndex].value;
									if (strUser)
										getlabels(strUser);
								}

							});

			    		}
			    		
			    	}	
	            }
			});
			
		}

		initfilelist();

		var region_count = 0;
		var testDebug = ["one","two","three"];
		
		var EmoList = new Array();
		var APNList = new Array();
		var PNNList = new Array();
		var regionID = new Array();
		var lastRegion = undefined;
		var currentIndex = 0;

		function LabelRegion(region, emoLabel, apnLabel, pnnLabel, id) {
			this.id = id;
			this.region = region;
			this.emoLabel = emoLabel;
			this.apnLabel = apnLabel;
			this.pnnLabel = pnnLabel;
		}

		LabelRegion.prototype.remove = function() {
			this.region.remove();
		}

		// var stage = new createjs.Stage('labelCanvas');
		// stage.scaleX = 1;
		// stage.scaleY = 1;

		$('#fileupload').bind('fileuploadstop', function (e, data) {
			for(var i = regionList.length-1; i >= 0; i--)
			{
				regionList[i].remove();
				regionList.splice(i, 1);
				lastRegion = undefined;
				currentIndex = -1;
			}
			refreshTable();
			wavesurfer.empty();
			wavesurfer.load('../uploads/'+username+ '/' + fileName);
			wavesurfer.on('ready', function() {
				//wavesurfer.play();
				// Enable creating regions by dragging
				wavesurfer.enableDragSelection({
					color: 'rgba(60,170,223, 0.7)'
				});
				var timeline = Object.create(WaveSurfer.Timeline);

				timeline.init({
					wavesurfer: wavesurfer,
					container: "#wave-timeline",
					primaryColor: 'blue',
					primaryFontColor: 'red',
					secondaryFontColor: 'red',
					notchPercentHeight: '90',
					height: '20',
					primaryLabelInterval: primaryD,
					secondaryLabelInterval: secondaryD
				});
			});

			$('#filelist').append($('<option>', {
				value: fileName,
				text: fileName
			}));	

			//getfilelist();

			$('#filelist').val(fileName);
			// var txt1 = "<option value="volvo">Volvo</option>";
		});

		$('#filelist').on('change', function() {
			$("#acceptsuggest").css("visibility", "hidden");  
	        $("#suggest").css("visibility", "hidden");
			$('#recomsec').text('Suggested labels: ');
			fileName=this.value;
		  	for(var i = regionList.length-1; i >= 0; i--)
			{
				regionList[i].remove();
				regionList.splice(i, 1);
				lastRegion = undefined;
				currentIndex = -1;
			}
			refreshTable();
			wavesurfer.empty();
			wavesurfer.load('../uploads/'+username+ '/' + this.value);
			wavesurfer.on('ready', function() {
				//wavesurfer.play();
				// Enable creating regions by dragging
				wavesurfer.enableDragSelection({
					color: 'rgba(120,120,120, 0.2)'
				});
				var timeline = Object.create(WaveSurfer.Timeline);

				timeline.init({
					wavesurfer: wavesurfer,
					container: "#wave-timeline",
					primaryColor: 'blue',
					primaryFontColor: 'white',
					secondaryFontColor: 'red',
					notchPercentHeight: '90',
					height: '20',
					primaryLabelInterval: primaryD,
					secondaryLabelInterval: secondaryD
				});
			});
			if(!wavesurfer.isPlaying())
				document.getElementById("play").innerHTML = "<i class=\"fa fa-play fa-fw\"></i>Play ";
			else
				document.getElementById("play").innerHTML = "<i class=\"fa fa-pause fa-fw\"></i>Pause ";

			//getlabels(this.value);
		});

	    region_count = 0;
	    var regionCreated = false;
		wavesurfer.on('region-created', function(region, e) {
			if (region_count === 0){
				region_count = 1;
				var newRegion = new LabelRegion(region, "", "", "", regionList.length + 1);
				//lastRegion = newRegion;
				regionList.push(newRegion);
				regionCreated = true;
			}
			else{
				region.remove();
			}
			//region_count += 1;
			// currentIndex = regionList.length - 1;
		});

		wavesurfer.on('region-update-end', function(region, e) {
			var index = findRegion(region);
			var diff = region.end - region.start;
			if (diff < 1) {
				// lastRegion.remove();
				if(regionList[index]){
					regionList[index].remove();
					regionList.splice(index, 1);
					lastRegion = undefined;
					currentIndex = -1;
				}
			}
			else {
				currentIndex = index - 1;
				lastRegion = regionList[index];
				//savelabels();
			}
			region_count = 0;
			// reDrawCanvas();  
			refreshTable();
		});

		wavesurfer.on('finish', function(progress) {
			document.getElementById("play").innerHTML = "<i class=\"fa fa-play fa-fw\"></i>Play ";
			// lastRegion = undefined;
			// currentIndex = -1;
			
			// refreshTable();
		});

		function findRegion(region) {
			for (var i = 0; i < regionList.length; i++) {
				if (regionList[i].region.id === region.id)
					return i;
			}
			return 0;
		}

		createTable(null);

		function createTable(tableData) {
			var table = document.getElementById('SegTable');
			while (table.childNodes.length > 1) {
				table.removeChild(table.lastChild);
			}
			var tableBody = document.createElement('tbody');

			var header = table.createTHead();
			var row = header.insertRow(0);     
			var cell1 = row.insertCell(0);
			var cell2 = row.insertCell(1);
			var cell3 = row.insertCell(2);
			var cell4 = row.insertCell(3);
			var cell5 = row.insertCell(4);
			var cell6 = row.insertCell(5);
			var cell7 = row.insertCell(6);
			cell1.innerHTML = "<b>ID</b>";
			cell2.innerHTML = "<b>Start</b>";
			cell3.innerHTML = "<b>End</b>";
			cell4.innerHTML = "<b>Emotion</b>";
			cell5.innerHTML = "<b>Arousal</b>";
			cell6.innerHTML = "<b>Valence</b>";
			cell7.innerHTML = "<b>Loop</b>";

			if(tableData !== null){
				tableData.forEach(function(rowData) {
					var row = document.createElement('tr');

					rowData.forEach(function(cellData) {
						var cell = document.createElement('td');
						cell.appendChild(document.createTextNode(cellData));
						row.appendChild(cell);
					});

					tableBody.appendChild(row);
				});
			}

			table.appendChild(tableBody);
		}

		function refreshTable() {
			var tableData = new Array();
			for (var i = 0; i < regionList.length; i++) {
				tableData[i] = [regionList[i].id, regionList[i].region.start.toFixed(2), regionList[i].region.end.toFixed(2), regionList[i].emoLabel, regionList[i].apnLabel, regionList[i].pnnLabel, regionList[i].region.loop];
			}

			createTable(tableData);

			$('#SegTable > tbody > tr').click(function(event) {
				if ($(this).index('tr') !== 0) {
					lastRegion = regionList[$(this).index('tr') - 1];
					var duration = wavesurfer.getDuration();
					wavesurfer.seekAndCenter(lastRegion.region.start / duration);
					currentIndex = $(this).index('tr') - 1;
					$('.row_selected').removeClass('row_selected');
					$(this).toggleClass('row_selected');
				}
			});
			if (lastRegion !== undefined) {
				$('.row_selected').removeClass('row_selected');
				var index = findRegion(lastRegion.region) + 1;
				if(index === -1)
					index = 0;
				$('#SegTable > tbody > tr:nth-child(' + index + ')').toggleClass('row_selected');
			}
			// savelabels();

		}



		function reDrawCanvas() {
			var c = document.getElementById("labelCanvas");
			var ctx = c.getContext("2d");
			ctx.lineWidth = 1;
			ctx.fillStyle = "#FF0000";
			ctx.clearRect(0, 0, c.width, c.height);
			// ctx.fillRect(0,0,20,10);

			var duration = wavesurfer.getDuration();
			var length = $('#wave').width();
			var rat = length / zoomLevel;
			var mult = length / rat;

			var aH = window.screen.availHeight;
			var aW = window.screen.availWidth;
			var ratio = aW / aH;
			ctx.scale(ratio, 1);

			for (var i = 0; i < regionList.length; i++) {
				// var shape = new createjs.Shape();
				var d1 = (regionList[i].region.start);
				console.log(d1);
				var d2 = ((regionList[i].region.end - regionList[i].region.start));
				var d3 = $('#labelCanvas').height();
				// shape.graphics.beginFill('red').drawRect(d1, 0, d2, d3);
				ctx.fillRect(d1, 50, d2, d3);
				// ctx.strokeText("Big smile!",d2,d3);
				// stage.addChild(shape);
				// stage.update();
			}
		}

		// var timeEvents = setInterval(function() {

		// }, 1000);

		$("body").dblclick(function() {
		  	lastRegion = undefined;
			currentIndex = -1;
			
			refreshTable();
		});

		$('#remove').click(function() {
			if (regionList.length > 0) {
				// lastRegion.remove();
				if (currentIndex < 0) {
					regionList[regionList.length - 1].remove();
					regionList.splice(regionList.length - 1, 1);
					lastRegion = regionList[regionList.length - 1];
					currentIndex = regionList.length - 1;
				} else {
					regionList[currentIndex].remove();
					regionList.splice(currentIndex, 1);
					if (regionList.length > 0) {	
						lastRegion = regionList[regionList.length - 1];
						currentIndex = regionList.length - 1;
					}
					else
					{
						lastRegion = undefined;
						currentIndex = 0;
					}
				}
				reorderIDs();
				refreshTable();
			}



		});

		function reorderIDs() {
			for (var i = 0; i < regionList.length; i++) {
				regionList[i].id = i + 1;
			}
		}

		$('#loop').click(function() {
			if (lastRegion !== undefined) {
				if (lastRegion.region.loop === true)
					lastRegion.region.loop = false;
				else
					lastRegion.region.loop = true;

				var duration = wavesurfer.getDuration();
				wavesurfer.seekAndCenter(lastRegion.region.start / duration);
				wavesurfer.playPause();
				refreshTable();
			}


		});


		$('#play').click(function() {

			if (lastRegion !== undefined) {
				var duration = wavesurfer.getDuration();
				wavesurfer.seekAndCenter(lastRegion.region.start / duration);
				wavesurfer.playPause();
			}
			else
				wavesurfer.playPause();

			if(!wavesurfer.isPlaying())
				document.getElementById("play").innerHTML = "<i class=\"fa fa-play fa-fw\"></i>Play ";
			else
				document.getElementById("play").innerHTML = "<i class=\"fa fa-pause fa-fw\"></i>Pause ";
			
		});

		$('#stop').click(function() {
	
			wavesurfer.stop();
			
			if(!wavesurfer.isPlaying())
				document.getElementById("play").innerHTML = "<i class=\"fa fa-play fa-fw\"></i>Play ";
			else
				document.getElementById("play").innerHTML = "<i class=\"fa fa-pause fa-fw\"></i>Pause ";
			
		});

		var EmoColorList = ['rgba(255, 0, 0, 0.3)', 'rgba(0, 255, 0, 0.3)', 'rgba(153, 50, 255, 0.3)', 'rgba(255, 150, 50, 0.3)', 'rgba(255, 255, 255, 0.3)', 'rgba(0, 0, 255, 0.5)', 'rgba(211,211,211, 0.3)'];

		$('#Anger').click(function() {
			if (lastRegion !== undefined) {
				lastRegion.emoLabel = "Anger";
				lastRegion.region.update({
					color: 'rgba(255, 0, 0, 0.3)'
				});
				refreshTable();
				savelabels();
			}
		});

		$('#Disgust').click(function() {
			if (lastRegion !== undefined) {
				lastRegion.emoLabel = "Disgust";
				lastRegion.region.update({
					color: 'rgba(0, 255, 0, 0.3)'
				});
				refreshTable();
				savelabels();
			}
		});

		$('#Fear').click(function() {
			if (lastRegion !== undefined) {
				lastRegion.emoLabel = "Fear";
				lastRegion.region.update({
					color: 'rgba(153, 50, 255, 0.3)'
				});
				refreshTable();
				savelabels();
			}
		});

		$('#Happy').click(function() {
			if (lastRegion !== undefined) {
				lastRegion.emoLabel = "Happy";
				lastRegion.region.update({
					color: 'rgba(255, 150, 50, 0.3)'
				});
				refreshTable();
				savelabels();
			}
		});

		$('#Neutral').click(function() {
			if (lastRegion !== undefined) {
				lastRegion.emoLabel = "Neutral";
				lastRegion.region.update({
					color: 'rgba(255, 255, 255, 0.3)'
				});
				refreshTable();
				savelabels();
			}
		
		});

		$('#Sad').click(function() {
			if (lastRegion !== undefined) {
				lastRegion.emoLabel = "Sad";
				lastRegion.region.update({
					color: 'rgba(0, 0, 255, 0.5)'
				});
				refreshTable();
				savelabels();
			}

		});

		$('#Active').click(function() {
			if (lastRegion !== undefined) {
				lastRegion.apnLabel = "Active";

				refreshTable();
				savelabels();
			}
		});
		$('#Passive').click(function() {
			if (lastRegion !== undefined) {
				lastRegion.apnLabel = "Passive";

				refreshTable();
				savelabels();
			}
		});
		$('#NeutralAPN').click(function() {
			if (lastRegion !== undefined) {
				lastRegion.apnLabel = "Neutral";

				refreshTable();
				savelabels();
	
			}

			
		});

		$('#Positive').click(function() {
			if (lastRegion !== undefined) {
				lastRegion.pnnLabel = "Positive";

				refreshTable();
				savelabels();
			}
		});
		$('#Negative').click(function() {
			if (lastRegion !== undefined) {
				lastRegion.pnnLabel = "Negative";

				refreshTable();
				savelabels();
			}
		});
		$('#NeutralPNN').click(function() {
			if (lastRegion !== undefined) {
				lastRegion.pnnLabel = "Neutral";

				refreshTable();
				savelabels();
			}
		});
		var saveCounter = 1;
		$('#savelocal').click(function() {
			var saveData = "ID\tStart\tEnd\tEmotion\tAPN\tPNN\r\n";
			for (var i = 0; i < regionList.length; i++) {
				saveData += [regionList[i].id + "\t" + regionList[i].region.start.toFixed(2)+ "\t"+ regionList[i].region.end.toFixed(2)+ "\t"+ regionList[i].emoLabel+ "\t"+ regionList[i].apnLabel+ "\t"+ regionList[i].pnnLabel+ "\r\n"];
			}
			var filename = fileName.slice(0,-4) + ".txt";
			download(filename, saveData.toString());
		
			
		
		});

		$('#savetoServer').click(function() {
			
			savelabels()
		
		});

		$('#loadfromServer').click(function() {
			
			getlabels(fileName);
		
		});




		$('#removefile').click(function() {
		
			wavesurfer.empty();
			removeFile();
			getfilelist();
			$("#filelist").val($("#filelist option:first").val()).change();
			//initfilelist();
		
		});


		function removeFile() {
			
			var fileN = $('#filelist').val();
			
			$.ajax({
			    url: "http://"+ipAd+"/labeling/"+username+"/removefile",
			    type: "POST",
			    crossDomain: true,
			    // contentType: 'application/json',  
			    data:  {fileName:fileN}
			});
		}


		function addTrainingData() {
			var index = findRegion(lastRegion.region);
			var emoval;
			var apnval;
			var pnnval;
			var regionstartval;
			var regionendval;

			var reqstatus = 'add';

			emoval = regionList[index].emoLabel;
			apnval = regionList[index].apnLabel;
			pnnval = regionList[index].pnnLabel;
			regionstartval = regionList[index].region.start;
			regionendval = regionList[index].region.end;
			var fileN = $('#filelist').val();
			
			$.ajax({
			    url: "http://"+ipAd+"/labeling/"+username+"/matlabrequest",
			    type: "POST",
			    crossDomain: true,
			    // contentType: 'application/json',  
			    data:  {fileName:fileN, regionstartval:regionstartval, regionendval:regionendval, emoval:emoval, apnval:apnval, pnnval:pnnval, reqstatus:reqstatus}
			});
		}

		function requestlabels() {
			var index = findRegion(lastRegion.region);
			var emoval;
			var apnval;
			var pnnval;
			var regionstartval;
			var regionendval;

			var reqstatus = 'analysis';
			emoval = regionList[index].emoLabel;
			apnval = regionList[index].apnLabel;
			pnnval = regionList[index].pnnLabel;
			regionstartval = regionList[index].region.start;
			regionendval = regionList[index].region.end;
			var fileN = $('#filelist').val();
			
			$.ajax({
			    url: "http://"+ipAd+"/labeling/"+username+"/matlabrequest",
			    type: "POST",
			    crossDomain: true,
			    // contentType: 'application/json',  
			    data:  {fileName:fileN, regionstartval:regionstartval, regionendval:regionendval, emoval:emoval, apnval:apnval, pnnval:pnnval, reqstatus:reqstatus}
			});
		}

		function savelabels() {
			var emolist = new Array();
			var apnlist = new Array();
			var pnnlist = new Array();
			var regionstartlist = new Array();
			var regionendlist = new Array();

			for(var i=0; i< regionList.length; i++){
				emolist[i] = regionList[i].emoLabel;
				apnlist[i] = regionList[i].apnLabel;
				pnnlist[i] = regionList[i].pnnLabel;
				regionstartlist[i] = regionList[i].region.start;
				regionendlist[i] = regionList[i].region.end;
			}

			$.ajax({
			    url: "http://"+ipAd+"/labeling/"+username,
			    type: "POST",
			    crossDomain: true,
			    // contentType: 'application/json',  
			    data:  {fileName:fileName, regionstartlist:regionstartlist, regionendlist:regionendlist, emolist:emolist, apnlist:apnlist, pnnlist:pnnlist}
			});
		}

		function matlabpost() {
			var emolist = new Array();
			var apnlist = new Array();
			var pnnlist = new Array();
			var regionstartlist = new Array();
			var regionendlist = new Array();

			for(var i=0; i< regionList.length; i++){
				emolist[i] = regionList[i].emoLabel;
				apnlist[i] = regionList[i].apnLabel;
				pnnlist[i] = regionList[i].pnnLabel;
				regionstartlist[i] = regionList[i].region.start;
				regionendlist[i] = regionList[i].region.end;
			}

			$.ajax({
			    url: "http://"+ipAd+"/labeling/"+username,
			    type: "POST",
			    crossDomain: true,
			    // contentType: 'application/json',  
			    data:  {fileName:fileName, regionstartlist:regionstartlist, regionendlist:regionendlist, emolist:emolist, apnlist:apnlist, pnnlist:pnnlist}
			});
		}

		function getlabels(currentfilename) {
			wavesurfer.clearRegions();
			regionList= new Array();
			var emolist = new Array();
			var apnlist = new Array();
			var pnnlist = new Array();
			var regionstartlist = new Array();
			var regionendlist = new Array();
			var stat;
			$.ajax({
			    url: "http://"+ipAd+"/labeling/"+username+"/"+currentfilename,
			    type: "GET",
			     crossDomain: true,
			    // contentType: 'application/json',  
			    data:  {regionstartlist:regionstartlist, regionendlist:regionendlist, emolist:emolist, apnlist:apnlist, pnnlist:pnnlist, status:stat},
			    success: function(data){
			    	if(data.status){
		             	for(var i=0; i< data.regionstartlist.length; i++){
		             		var curColor = EmoColorList[findEmoColor(data.emolist[i])];
		             		reg_opt = {
							    start: data.regionstartlist[i],
							    end: data.regionendlist[i],
							    color: curColor 
							};
							var region = wavesurfer.addRegion(reg_opt);
							while(regionCreated === false){};
							region_count = 0;
		             		regionList[findRegion(region)].emoLabel = data.emolist[i];
		             		regionList[findRegion(region)].apnLabel = data.apnlist[i];
		             		regionList[findRegion(region)].pnnLabel = data.pnnlist[i];
		             		regionCreated = false;
						}
						refreshTable();
					}
	            }
			});
			
		}

		

		function findEmoColor(emotion){
			var EmoInd;
			switch(emotion) {
			    case "Anger":
			        EmoInd = 0;
			        break;
			    case "Disgust":
			        EmoInd = 1;
			        break;
			    case "Fear":
			        EmoInd = 2;
			        break;
			    case "Happy":
			        EmoInd = 3;
			        break;
			    case "Neutral":
			        EmoInd = 4;
			        break;
			    case "Sad":
			        EmoInd = 5;
			        break;
			    default:
			        EmoInd = 6;
			}
			return EmoInd;
		}

			
		function download(filename, text) {
		  var element = document.createElement('a');
		  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
		  element.setAttribute('download', filename);

		  element.style.display = 'none';
		  document.body.appendChild(element);

		  element.click();

		  document.body.removeChild(element);
		}

		
	}



	var slider = document.querySelector('#slider');

	slider.oninput = function() {
		zoomLevel = Number(slider.value);
		wavesurfer.zoom(zoomLevel);
	};



});

$.fn.redraw = function(){
  $(this).each(function(){
    var redraw = this.offsetHeight;
  });
};
