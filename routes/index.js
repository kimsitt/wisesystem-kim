// var express = require('express');
var path = require('path');
var File = require('../public/js/user.js').File;
var fs = require('fs');
var spawn = require('child_process').spawn;
var zerorpc = require("zerorpc");

// app/routes.js
module.exports = function(app, passport, upload, dirname, io) {

	var clientpy = new zerorpc.Client();
	clientpy.connect("tcp://127.0.0.1:4242");
	var people = {}; 

	// app.post('/wise/send/removeindex' , isAuthenticated, function(req, res) {
	// 		console.log("test")
	// 		console.log(req.body)
	// 		console.log(req.body.index)
	// 		res.sendStatus(200);
	// });

	io.on("connection", function (client) {  



	    client.on("join", function(name){
	        people[client.id] = name;
	        client.emit("console", "You have connected to the server.");
	        io.sockets.emit("news", name + " has joined the server.");

	        
	    });

	    client.on("addtoTraining", function(username, modelname, fileN, emoval, apnval, pnnval, regionstartval, regionendval){
	    	// console.log(emoval + ' ' + apnval);
	        clientpy.invoke("addtoTraining", username, modelname, fileN, emoval, apnval, pnnval, regionstartval, regionendval, function(error, res, more) {
			    // console.log(res);
			    client.emit("console", res);
			});
		});

		client.on("removefromTraining", function(username, modelname, indexlist){
	    	clientpy.invoke("removefromTraining", username, modelname, indexlist, function(error, res, more) {
			    client.emit("fileremoved", res);
			});
	        
		});

		client.on("requestlabel", function(username, modelname, fileN, regionstartval, regionendval){
	    	// console.log(emoval + ' ' + apnval);
	        clientpy.invoke("classify", username, modelname, fileN, regionstartval, regionendval, function(error, emo, apn, pnn, more) {
			    // console.log(res);
			    client.emit("suggestion", emo, apn, pnn);
			});
			// clientpy.invoke("printData", username, function(error, res, more) {
			//     console.log(res);
			//     client.emit("suggestion", res);
			// });
		});

		client.on("createmodel", function(username,modelname){
	    	// console.log(emoval + ' ' + apnval);
	        clientpy.invoke("createEmotionModel", username, modelname,function(error, res, more) {
			    // console.log(res);
			    client.emit("console", res);
			});
			// clientpy.invoke("printData", username, function(error, res, more) {
			//     console.log(res);
			//     client.emit("suggestion", res);
		});
	
		client.on("addmodel", function(username, modelname){
	    	// console.log(emoval + ' ' + apnval);
	        clientpy.invoke("addmodel", username, modelname, function(error, res, more) {
			    // console.log(res);
			    client.emit("console", res);
			});
			
		});

		client.on("inspectmodel", function(username, modelname){
	    	// console.log(emoval + ' ' + apnval);
	        clientpy.invoke("inspectmodel", username, modelname, function(error, res,  more) {
			    // console.log(res);
			    client.emit("modellist", res);
			});
			
		});

		client.on("removemodel", function(username, modelname){
	    	// console.log(emoval + ' ' + apnval);
	        clientpy.invoke("removemodel", username, modelname, function(error, res, more) {
			    // console.log(res);
			    client.emit("console", res);
			});
			
		});

	    client.on("disconnect", function(){
	        
	    });



	});

	//Spawn Matlab
	// m = spawn('matlab', ['-r', "cd matlab; s = server('localhost', 5000); s.receive(); s.send('This is MatLab'); loadFeatures;"]);

	//   var matlabclient;

	//   client = require(path.normalize('./../public/js/config/matlabcom.js'));
	//   setTimeout(function(){
	//    	  host='localhost';
	// 	  port=5000;
	// 	  matlabclient = new client(host, port, io);
	// 	  matlabclient.receive();

 //  	  }, 15000);
	 

 //    setTimeout(function(){
	//     // matlabclient.send('This is NodeJS\n');
	//     // matlabclient.send('This is NodeJS\n');
	//   }, 16000);

	app.get('/', function(req, res) {
		var str = path.normalize(__dirname + '/../public/htmls/login.html');
		res.sendFile(str);
		// res.render('profile.ejs'); // load the index.ejs file
	});

	upload.on('end', function (fileInfo, req, res) {
	    console.log("Uploaded file.");
	    console.log(fileInfo.name);

	    var newFile = new File();
	
		newFile.username = req.user.username;
		newFile.filename = fileInfo.name;

		// save the user
		newFile.save(function(err) {
			if (err) {
				return next(err);
			}
		});

	});

	/// Redirect all to home except post
	app.get('/upload', function( req, res ){
	    res.redirect('/');
	});

	app.put('/upload', function( req, res ){
	    res.redirect('/');
	});

	app.delete('/upload', function( req, res ){
	    res.redirect('/');
	});

	app.use('/upload', isAuthenticated, function(req, res, next) {
		upload.fileHandler({
			uploadDir: function() {

				return path.normalize(dirname + '/public/uploads/' + req.user.username + '/')
			},
			uploadUrl: function() {
				return '/uploads'
			}
		})(req, res, next);
	});

	// =====================================
	// LOGIN ===============================
	// =====================================
	// show the login form
	app.get('/login', function(req, res) {

		// render the page and pass in any flash data if it exists
		// res.sendFile('../public/htmls/login.html'); 
		// var str = path.normalize(__dirname + '/../public/htmls/login.html');
		// res.sendFile(str);
		res.render('login.ejs', {
			message: req.flash('loginMessage')
		});
	});

	// process the login form
	// app.post('/login', passport.authenticate('local-login', {
	// 	successRedirect : '/labeling', // redirect to the secure profile section
	// 	failureRedirect : '/login', // redirect back to the signup page if there is an error
	// 	failureFlash : true // allow flash messages
	// }));

	app.post('/login', function(req, res, next) {
		passport.authenticate('local-login', function(err, user, info) {
			if (err) {
				return next(err);
			}
			// Redirect if it fails
			if (!user) {
				return res.redirect('/login');
			}
			req.logIn(user, function(err) {
				if (err) {
					return next(err);
				}
				// Redirect if it succeeds
				return res.redirect('/labeling/' + user.username);
			});
		})(req, res, next);
	});
	// app.post('/login', passport.authenticate('login', {
	// 	successRedirect : '/userpage', // redirect to the secure profile section
	// 	failureRedirect : '/login', // redirect back to the signup page if there is an error
	// 	failureFlash : true // allow flash messages
	// }));

	// =====================================
	// SIGNUP ==============================
	// =====================================
	// show the signup form
	app.get('/signup', function(req, res) {

		// render the page and pass in any flash data if it exists
		res.render('signup.ejs', {
			message: req.flash('signupMessage')
		});
		// var str = path.normalize(__dirname + '/../public/htmls/signup.html');
		// res.sendFile(str);
	});

	// process the signup form
	// app.post('/signup', passport.authenticate('local-signup', {
	// 	successRedirect : '/labeling', // redirect to the secure profile section
	// 	failureRedirect : '/signup', // redirect back to the signup page if there is an error
	// 	failureFlash : true // allow flash messages
	// }));

	app.post('/signup', function(req, res, next) {
		passport.authenticate('local-signup', function(err, user, info) {
			if (err) {
				return next(err);
			}
			// Redirect if it fails
			if (!user) {
				return res.redirect('/signup');
			}
			req.logIn(user, function(err) {
				if (err) {
					return next(err);
				}
				// Redirect if it succeeds
				clientpy.invoke("copyLDC", user.username, function(error, res, more) {
				    // console.log(res);
				    // client.emit("console", res);
				    if(res){
				    	clientpy.invoke("addmodel", user.username, "default", function(error, res, more) {
						    // console.log(res);
						    // client.emit("console", res);
						});
				    	clientpy.invoke("createEmotionModel", user.username, "LDC", function(error, res, more) {
							
						});
				    }
				});
				return res.redirect('/labeling/' + user.username);
			});
		})(req, res, next);
	});

	// app.post('/signup', passport.authenticate('signup', {
	// 	successRedirect : '/userpage', // redirect to the secure profile section
	// 	failureRedirect : '/signup', // redirect back to the signup page if there is an error
	// 	failureFlash : true // allow flash messages
	// }));


	// =====================================
	// PROFILE SECTION =========================
	// =====================================
	// we will want this protected so you have to be logged in to visit
	// we will use route middleware to verify this (the isLoggedIn function)
	app.get('/userpage', isAuthenticated, function(req, res) {

		res.render('profile.ejs', {
			user: req.user // get the user out of session and pass to template
		});
	});


	app.get('/labeling/:userid', isAuthenticated, function(req, res) {
		//res.render('signup.ejs', { message: req.flash('signupMessage') });

		res.render('labeling.ejs', {
			user: req.user.username // get the user out of session and pass to template
		});
	});

	app.get('/labeling/:userid/model/list', isAuthenticated, function(req, res) {
		var userid = req.params.userid;
		//res.render('signup.ejs', { message: req.flash('signupMessage') });
		//console.log(currentfilename);
		
		//console.log(userid);
		fs.readdir("./python/usermodels/" + userid, function(err, list) {
			if (err) return "Dir error";
			var results = [];
			for (var i = 0; i < list.length; i++) {
				var file = list[i];
				var substr = file.substring(file.length-3, file.length);
				// console.log(substr);
				if (substr == "npz"){
					// console.log(file.substring(0, file.length-4));
					results.push(file.substring(0, file.length-4));
				}
			}
			res.json({
				results: results
			});
		});
		

	});

	app.get('/labeling/:userid/:currentfilename', isAuthenticated, function(req, res) {
		var userid = req.params.userid;
		var currentfilename = req.params.currentfilename;
		//res.render('signup.ejs', { message: req.flash('signupMessage') });
		//console.log(currentfilename);
		if (currentfilename !== "filelist") {
			File.findOne({
				'username': userid,
				'filename': currentfilename
			}, function(err, result) {
				if (err) {
					console.log("Error!")
				}

				if (result) {
					// console.log(result.username + " requests labels.");
					res.json({
						emolist: result.emolist,
						apnlist: result.apnlist,
						pnnlist: result.pnnlist,
						regionstartlist: result.regionstartlist,
						regionendlist: result.regionendlist,
						status: true
					});
				} else {
					res.json({
						status: false
					});
				}

			});
		} else {
			//console.log(userid);
			fs.readdir("./public/uploads/" + userid, function(err, list) {
				if (err) return "Dir error";
				var results = [];
				for (var i = 0; i < list.length; i++) {
					var file = list[i];
					//console.log(file);
					results.push(file);
				}
				res.json({
					results: results
				});
			});
		}

	});

	app.get('/notlogged', function(req, res) {
		var str = path.normalize(__dirname + '/../public/htmls/notlogged.html');
		res.sendFile(str);
	});

	app.post('/labeling/:userid/matlabrequest', isAuthenticated, function(req, res) {
		if(!req.body.emoval)
			req.body.emoval = "null";
		if(!req.body.apnval)
			req.body.apnval = "null";
		if(!req.body.pnnval)
			req.body.pnnval = "null";
		if(!req.body.fileName)
			req.body.fileName = "null";

		// client.invoke("addtoTraining", req.user.username,req.body.fileName,req.body.emoval,req.body.apnval, req.body.pnnval,req.body.regionstartval, req.body.regionendval, function(error, res, more) {
		//     console.log(res);
		// });
		// matlabclient.send(req.user.username+'_*_'+req.body.fileName+'_*_'+req.body.emoval+'_*_'+req.body.apnval+'_*_'+ req.body.pnnval+ '_*_'+ req.body.regionstartval+ '_*_'+ req.body.regionendval + '_*_'+ req.body.reqstatus);
		res.sendStatus(200);
	});

	

	app.post('/labeling/:userid/removefile', isAuthenticated, function(req, res) {
		if(!req.body.fileName)
			req.body.fileName = "null";
		
		File.findOne({
			'username': req.body.username,
			'filename': req.body.fileName
		}, function(err, result) {
			if (err) {
				console.log("Error!")
			}

			if (result) {
				result.remove();
				console.log(result.username + " deleted.");
				
			} else {
				console.log('File is not found.')
			}

		});

		// matlabclient.send(req.user.username+'_*_'+req.body.fileName+'_*_remove_*_');
		// console.log(path.normalize('./public/uploads/' + req.user.username +'/'+ req.body.fileName));
		fs.unlink(path.normalize('./public/uploads/' + req.user.username +'/'+ req.body.fileName), function(err) {
		  if(err) return console.log(err);
		  // console.log('file deleted successfully');
	      res.send ({
	        status: "200",
	        responseType: "string",
	        response: "success"
	      });     
	    });
	  
	});

	app.post('/labeling/:userid', isAuthenticated, function(req, res) {
		//matlabclient.send(req.user.username+' '+req.body.fileName+' '+req.body.emolist+' '+req.body.apnlist+' '+ req.body.pnnlist+ ' '+ req.body.regionstartlist+ ' '+ req.body.regionendlist);
		// console.log(User.findOne({'local.username': req.user.local.username}));
		File.findOne({
			'filename': req.body.fileName,
			'username': req.user.username
		}, function(err, result) {
			if (err) { /* handle err */ }

			if (result) {
				// console.log(result.username + " has sent labels.");
				result.emolist = req.body.emolist;
				result.apnlist = req.body.apnlist;
				result.pnnlist = req.body.pnnlist;
				result.regionstartlist = req.body.regionstartlist;
				result.regionendlist = req.body.regionendlist;
				result.save(function(err) {
					if (err) {
						return next(err);
					}
				});
			} else {
				var newFile = new File();

				// set the user's local credentials
				// newUser.id = ObjectId();
				newFile.username = req.user.username;
				newFile.filename = req.body.fileName;
				newFile.emolist = req.body.emolist;
				newFile.emolist = req.body.emolist;
				newFile.apnlist = req.body.apnlist;
				newFile.pnnlist = req.body.pnnlist;
				newFile.regionstartlist = req.body.regionstartlist;
				newFile.regionendlist = req.body.regionendlist;

				// save the file
				newFile.save(function(err) {
					if (err) {
						return next(err);
					}
				});
			}

		});
		res.sendStatus(200);

	});

	// =====================================
	// LOGOUT ==============================
	// =====================================
	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

	app.all('*', function(req, res) {
	  res.redirect("http://localhost:3000/"); //Changed this to the local host
	});
};

function isAuthenticated(req, res, next) {

	// do any checks you want to in here

	// CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
	// you can do this however you want with whatever variables you set up
	//console.log("test");
	if (req.user)
		return next();

	// IF A USER ISN'T LOGGED IN, THEN REDIRECT THEM SOMEWHERE
	res.redirect('/login');
}