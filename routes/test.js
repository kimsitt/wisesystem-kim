var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/Emre', function(req, res, next) {
  res.json({
  	foo: 'bar',
  	time: Date.now()
  });
});


router.put('/Emre', function(req, res, next) {
  var name = req.body && req.body.name || 'unknown';
  console.log('Arrived data: ', name, req.query);
  res.json({
  	status: true,
  	time: Date.now()
  });
});

module.exports = router;
