import zerorpc
from pyAudioAnalysis import audioBasicIO, audioFeatureExtraction
import numpy as np 
import scipy.io.wavfile as wavfile
from aubio import source, pitch
from scikits.samplerate import resample
from scikits.talkbox import lpc
from scipy.signal import lfilter, hamming, freqz, find_peaks_cwt
import math
from tempfile import TemporaryFile
from sklearn.svm import SVC, SVR, LinearSVC
from sklearn.cross_validation import train_test_split
import pandas as pd
from sklearn.feature_selection import VarianceThreshold
from sklearn.feature_selection import RFE
from sklearn.feature_selection import RFECV
from sklearn.cross_validation import StratifiedKFold
from sklearn.utils.multiclass import type_of_target
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.feature_selection import SelectKBest, chi2, GenericUnivariateSelect
from sklearn.metrics import hinge_loss
from minepy import MINE
import sklearn
import os

indexlist = [False, True, False, True];
print indexlist
indexlist = np.array(indexlist, dtype=bool);
indexlist = np.invert(indexlist)
print indexlist
npzfile = np.load("usermodels/"+"at"+"/"+"asd"+".npz");
y = npzfile['y'];
filenames = (npzfile['filename']);
starttimes = npzfile['start'];
stoptimes = npzfile['stop'];
x = npzfile['x'];
print x.shape;
x = x[indexlist];
print x.shape;
# if not os.path.exists("usermodels/at"):
#     os.makedirs("at")






# print sklearn.__version__
# npzfile = np.load("usermodels/atar_classifier.npy");

# print npzfile
# x = np.arange(450)
# y = np.array([1,2,3]);
	
# np.savez("outfile", x=x, y=y)
# npzfile = 0;
# try:
# 	npzfile = np.load("usermodels/at");
# 	print npzfile['y']

# 	x2 = 2*np.arange(450)
# 	y2 = 2*np.array([1,2,3]);

# 	x = np.vstack((npzfile['x'], x2)) 
# 	y = np.vstack((npzfile['y'], y2))

# 	print x.shape, y.shape

# 	np.savez("outfile", x=x, y=y)

# 	npzfile = np.load("outfile.npz");

# 	print npzfile['y']

# except IOError as e:
#     print "I/O error({0}): {1}".format(e.errno, e.strerror)
# except:
#     print "Unexpected error:", sys.exc_info()[0]
#     raise





