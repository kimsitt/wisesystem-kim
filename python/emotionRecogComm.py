import zerorpc
from pyAudioAnalysis import audioBasicIO, audioFeatureExtraction
import numpy as np 
import scipy.io.wavfile as wavfile
from aubio import source, pitch
from scikits.samplerate import resample
from scikits.talkbox import lpc
from scipy.signal import lfilter, hamming, freqz, find_peaks_cwt
import math
from tempfile import TemporaryFile
from sklearn.svm import SVC, SVR, LinearSVC
from sklearn.cross_validation import train_test_split
import pandas as pd
from sklearn.feature_selection import VarianceThreshold
from sklearn.feature_selection import RFE
from sklearn.feature_selection import RFECV
from sklearn.cross_validation import StratifiedKFold
from sklearn.utils.multiclass import type_of_target
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.feature_selection import SelectKBest, chi2, GenericUnivariateSelect
from sklearn.metrics import hinge_loss
from minepy import MINE
from sklearn.externals import joblib
import pickle
import os
import json
from shutil import copyfile


#from cloud.serialization import cloudpickle

emotionlist = ["Anger" , "Disgust", "Fear", "Happy", "Neutral", "Sad"];
arousallist = ["Active", "Passive", "Neutral"];
valancelist = ["Positive", "Negative", "Neutral"];


name =0;
class emotionRPC(object):
	enableDD = True

	def computeFormants(self,x, fs):
		Fs=10000;
		x1=resample(x, float(Fs)/float(fs), 'sinc_best');
		
		ncoeff = 2 + Fs / 1000;

		# Get LPC.
		A, e, k = lpc(x1, ncoeff)

		[f, h]= freqz(1, A, 512);
		f = Fs * f / (2*np.pi)

		gain = 20*np.log10(np.abs(h)+np.finfo(float).eps);

		# Get roots.
		rts = np.roots(A)
		rts = [r for r in rts if np.imag(r) > 0.01]

	    # Get angles.
		angz = np.arctan2(np.imag(rts), np.real(rts))

		frqs = sorted(angz * (Fs / (2 * math.pi)));
		indices = np.argsort(angz * (Fs / (2 * math.pi)));

		rts = np.array(rts)

		
		frqs_bw = -1/2*(Fs/(2*math.pi))*np.log(abs(rts[indices]));

		return frqs[0:4], frqs_bw[0:4]

	def calculateStatistics(self,input):
		featureMEAN = np.nanmean(input, axis=1);
		featureSTD = np.nanstd(input, axis=1);
		featureMIN = np.nanmin(input, axis=1);
		featureMAX = np.nanmax(input, axis=1);
		featureRANGE = featureMAX-featureMIN;
		features = np.concatenate([featureMEAN, featureSTD, featureMIN, featureMAX, featureRANGE], axis=0);
		return features;

	def calculateStatisticsVec(self,input):
		featureMEAN = np.nanmean(input);
		featureSTD = np.nanstd(input);
		featureMIN = np.nanmin(input);
		featureMAX = np.nanmax(input);
		featureRANGE = featureMAX-featureMIN;
		features = [featureMEAN, featureSTD, featureMIN, featureMAX, featureRANGE];
		return features;

	def extractFeatures(self,filename, start, stop):
		# praatFormants(filename);
		[Fs, x] = audioBasicIO.readAudioFile(filename);

		# print x.shape, 
		x = x[int(np.ceil(start*Fs)):int(np.ceil(stop*Fs))];
		# print x.shape

		# Parameters
		win_s = int(np.ceil(0.060*Fs));
		hop_s = int(np.ceil(0.010*Fs));
		tolerance = 0.8
		thr_amp = 0.02;
		thr_rat = 0.2;
		maxval = 1;
		minval = -1;

		# Extract features using pyAudioanalysis library
		F = audioFeatureExtraction.stFeatureExtraction(x, Fs, win_s, hop_s);

		F = F[0:21][:];
		F_diff1 = np.diff(F);
		F_diff2 = np.diff(F_diff1);

		FSt = self.calculateStatistics(F);
		FSt_diff1 = self.calculateStatistics(F_diff1);
		FSt_diff2 = self.calculateStatistics(F_diff2);

		# s = source(filename, Fs, hop_s)
		# samplerate = s.samplerate

		Fs, x = wavfile.read(filename);

		pitch_o = pitch("yin", win_s, hop_s, Fs)
		pitch_o.set_unit("midi")
		pitch_o.set_tolerance(tolerance)

		pitches = []
		confidences = []
		pitch_local=0;

		# print np.min(x), np.max(x), (x - np.min(x)) 
		x = np.array(x).astype(float);
		# print x.shape, 
		x = x[int(np.ceil(start*Fs)):int(np.ceil(stop*Fs))];
		# print x.shape

		x = (x/np.iinfo(np.int16).max)
		# x = 2*(x - np.min(x)) / (np.max(x) - np.min(x))-1;
		# x = (x / (maxval - minval)) + minval
		# print 
		# total number of frames read
		# total_frames = 0
		total_frames = int(np.floor((x.size-win_s)/hop_s)+1);

		# sample marker
		sample_marker = np.zeros((x.size,));
		sample_marker[np.abs(x) > thr_amp] = 1;

		formant_fq = np.zeros((4,total_frames));
		formant_bw = np.zeros((4,total_frames));
		pitches = np.zeros((total_frames,));

		counter2 = 0;
		counter = 0;
		# frame marker
		frame_marker = np.zeros((total_frames,));
		for frame in range(0,total_frames):
			head = np.uint32((frame)*hop_s);
			tail = np.uint32((frame)*hop_s+win_s);
			x_frame = np.array(x[head:tail], dtype=np.float32);
			sample_marker_frame = sample_marker[head:tail];
			# print head, tail, x_frame.shape, np.count_nonzero(sample_marker_frame)/float(win_s)
			if np.count_nonzero(sample_marker_frame)/float(win_s) > thr_rat:	
				formant_fq[:, frame], formant_bw[:, frame] = self.computeFormants(x_frame, Fs);
				pitch_local = pitch_o(x_frame)[0]
				pitches[frame] = pitch_local
				# print frqs, ffreq_bw
				counter2 = counter2 + 1;
				# if counter2 == 5:
				# 	break;
		
		pitches_delta = np.diff(pitches);
		pitches_ddelta = np.diff(pitches_delta);

		pitches[pitches == 0] = np.nan
		pitches_delta[pitches_delta == 0] = np.nan
		pitches_ddelta[pitches_ddelta == 0] = np.nan

		pitches_stats = self.calculateStatisticsVec(pitches);
		pitches_delta_stats = self.calculateStatisticsVec(pitches_delta);
		pitches_ddelta_stats = self.calculateStatisticsVec(pitches_ddelta);

		pitch_stats = np.concatenate([pitches_stats, pitches_delta_stats, pitches_ddelta_stats], axis=0);

		formant_fq_delta = np.diff(formant_fq);
		formant_bw_delta = np.diff(formant_bw);

		formant_fq_ddelta = np.diff(formant_fq_delta);
		formant_bw_ddelta = np.diff(formant_bw_delta);

		formant_fq[formant_fq == 0] = np.nan
		formant_bw[formant_bw == 0] = np.nan
		formant_fq_delta[formant_fq_delta == 0] = np.nan
		formant_bw_delta[formant_bw_delta == 0] = np.nan
		formant_fq_ddelta[formant_fq_ddelta == 0] = np.nan
		formant_bw_ddelta[formant_bw_ddelta == 0] = np.nan

		formant_fq_stats = self.calculateStatistics(formant_fq);
		formant_bw_stats = self.calculateStatistics(formant_bw);

		formant_fq_delta_stats = self.calculateStatistics(formant_fq_delta);
		formant_bw_delta_stats = self.calculateStatistics(formant_bw_delta);

		formant_fq_ddelta_stats = self.calculateStatistics(formant_fq_ddelta);
		formant_bw_ddelta_stats = self.calculateStatistics(formant_bw_ddelta);

		formant_stats = np.concatenate([formant_fq_stats, formant_bw_stats,  \
			formant_fq_delta_stats, formant_bw_delta_stats, \
			formant_fq_ddelta_stats, formant_bw_ddelta_stats], axis=0);

		if self.enableDD:
			features = np.concatenate([FSt, FSt_diff1, FSt_diff2, formant_stats, pitch_stats], axis=0);
		else:
			features = np.concatenate([FSt, FSt_diff1, formant_fq_stats, formant_bw_stats,  \
			formant_fq_delta_stats, formant_bw_delta_stats, pitches_stats, pitches_delta_stats], axis=0);

		return features;

	def createSingleModel(self, username, modelname, X_train, y_train, emotion, mode):
		# print y_train
		y_train[y_train!=emotion]=0;
		y_train[y_train==emotion]=1;
		y_train = y_train.astype(int);

		# print X_train.shape

		classifier = SVC(kernel='rbf',C=8, gamma=1./450., probability=True, class_weight=None, verbose=False);

		classifier = classifier.fit(X_train, y_train);

		name = joblib.dump(classifier, "usermodels/"+username+"/"+modelname+'_classifier'+str(emotion)+str(mode)+'.pkl');

		# y_confidence = classifier.predict_log_proba(x_test);
		# return y_confidence[:,1];

	def createEmotionModel(self, username, modelname):
		try:
			# print "usermodels/"+username+"/"+modelname+".npz",
			npzfile = np.load("usermodels/"+username+"/"+modelname+".npz");
			# print npzfile['x'].shape, npzfile['y'].shape

			y_train = npzfile['y'];

			y_trainemo = y_train[1:,0];
			y_trainapn = y_train[1:,1];
			y_trainpnn = y_train[1:,2];

			counter = np.bincount(y_trainemo);
			# print y_train, counter;

			for i in range(1, 7):
				if (counter[i] < 2) :
					return "Not enough samples, please add at least 2 samples from each class."


			X_train = npzfile['x'];
			X_train = X_train[1:,:];

			# normalization between [0,1]
			fmin = np.min(X_train, axis=0);
			fmax = np.max(X_train, axis=0);

			rang = (fmax-fmin);
			rang[rang == 0.0] = 1;

			X_train = (X_train - fmin) / (rang);

			X_train[1*np.isnan(X_train) == 1] = 0;

			np.savez("usermodels/"+username+"/"+modelname+"minmax.npz", fmin=fmin, fmax=fmax);
			
			self.createSingleModel(username, modelname,X_train.copy(), y_trainemo.copy(), 1, 1);
			self.createSingleModel(username, modelname,X_train.copy(), y_trainemo.copy(), 2, 1);
			self.createSingleModel(username, modelname,X_train.copy(), y_trainemo.copy(), 3, 1);
			self.createSingleModel(username, modelname,X_train.copy(), y_trainemo.copy(), 4, 1);
			self.createSingleModel(username, modelname,X_train.copy(), y_trainemo.copy(), 5, 1);
			self.createSingleModel(username, modelname,X_train.copy(), y_trainemo.copy(), 6, 1);

			self.createSingleModel(username, modelname,X_train.copy(), y_trainapn.copy(), 1, 2);
			self.createSingleModel(username, modelname,X_train.copy(), y_trainapn.copy(), 2, 2);
			self.createSingleModel(username, modelname,X_train.copy(), y_trainapn.copy(), 3, 2);

			self.createSingleModel(username, modelname,X_train.copy(), y_trainpnn.copy(), 1, 3);
			self.createSingleModel(username, modelname,X_train.copy(), y_trainpnn.copy(), 2, 3);
			self.createSingleModel(username, modelname,X_train.copy(), y_trainpnn.copy(), 3, 3);

			return "Emotion model has been updated!"
		except IOError as e:
			return "Emotion model cannot be updated!"
			print "I/O error({0}): {1}".format(e.errno, e.strerror)
		except:
			return "Emotion model cannot be updated!"
			raise


	def classify(self, username, modelname, filename, start, stop):
		try:


			classifier1 = joblib.load("usermodels/"+username+"/"+modelname+'_classifier11.pkl');
			classifier2 = joblib.load("usermodels/"+username+"/"+modelname+'_classifier21.pkl');
			classifier3 = joblib.load("usermodels/"+username+"/"+modelname+'_classifier31.pkl');
			classifier4 = joblib.load("usermodels/"+username+"/"+modelname+'_classifier41.pkl');
			classifier5 = joblib.load("usermodels/"+username+"/"+modelname+'_classifier51.pkl');
			classifier6 = joblib.load("usermodels/"+username+"/"+modelname+'_classifier61.pkl');

			classifier1apn = joblib.load("usermodels/"+username+"/"+modelname+'_classifier12.pkl');
			classifier2apn = joblib.load("usermodels/"+username+"/"+modelname+'_classifier22.pkl');
			classifier3apn = joblib.load("usermodels/"+username+"/"+modelname+'_classifier32.pkl');

			classifier1pnn = joblib.load("usermodels/"+username+"/"+modelname+'_classifier13.pkl');
			classifier2pnn = joblib.load("usermodels/"+username+"/"+modelname+'_classifier23.pkl');
			classifier3pnn = joblib.load("usermodels/"+username+"/"+modelname+'_classifier33.pkl');
			
			# classifier = np.load("usermodels/"+username+"_classifier.npy");
			# print username, filename, classifier
			filepath = "../public/uploads/"+username+"/"+filename;
			features = self.extractFeatures(filepath, start, stop);
			npzfile = np.load("usermodels/"+username+"/"+modelname+"minmax.npz");

			fmin = npzfile["fmin"];
			fmax = npzfile["fmax"];

			features = (features - fmin) / (fmax-fmin);

			features[1*np.isnan(features) == 1] = 0;

			y_confidence1 = classifier1.predict_log_proba(features);
			y_confidence2 = classifier2.predict_log_proba(features);
			y_confidence3 = classifier3.predict_log_proba(features);
			y_confidence4 = classifier4.predict_log_proba(features);
			y_confidence5 = classifier5.predict_log_proba(features);
			y_confidence6 = classifier6.predict_log_proba(features);

			y_confidence1apn = classifier1apn.predict_log_proba(features);
			y_confidence2apn = classifier2apn.predict_log_proba(features);
			y_confidence3apn = classifier3apn.predict_log_proba(features);

			y_confidence1pnn = classifier1.predict_log_proba(features);
			y_confidence2pnn = classifier2.predict_log_proba(features);
			y_confidence3pnn = classifier3.predict_log_proba(features);

			confidences = np.column_stack([y_confidence1[:,1], y_confidence2[:,1], \
			y_confidence3[:,1], y_confidence4[:,1], y_confidence5[:,1], y_confidence6[:,1]]);

			confidencesapn = np.column_stack([y_confidence1apn[:,1], y_confidence2apn[:,1], \
			y_confidence3apn[:,1]]);

			confidencespnn = np.column_stack([y_confidence1pnn[:,1], y_confidence2pnn[:,1], \
			y_confidence3pnn[:,1]]);

			# print confidences
			predictions = np.argmax(confidences, axis=1);
			predictions= predictions.astype(int);
			predictions = np.asscalar(predictions);

			predictionsapn = np.argmax(confidencesapn, axis=1);
			predictionsapn= predictionsapn.astype(int);
			predictionsapn = np.asscalar(predictionsapn);

			predictionspnn = np.argmax(confidencespnn, axis=1);
			predictionspnn= predictionspnn.astype(int);
			predictionspnn = np.asscalar(predictionspnn);

			# print predictions, predictionsapn, predictionspnn
			return emotionlist[predictions] + ', ' + arousallist[predictionsapn] + ', ' + valancelist[predictionspnn]

		except IOError as e:
			print "I/O error({0}): {1}".format(e.errno, e.strerror)
		except:
			print "Unexpected error:"
			raise

	def addmodel(self, username, modelname):
		# print username, modelname
		try:
			if not os.path.exists("usermodels/"+username):
				os.makedirs("usermodels/"+username)
			features = np.zeros((1,450),dtype=np.float);
			y = np.zeros((1,3),dtype=np.int);
			np.savez("usermodels/"+username+"/"+modelname+".npz", x=features, y=y, filename="filename", start=0.0, stop=10.0);
			return modelname+" has been created."
		except IOError as e:
			print "I/O error({0}): {1}".format(e.errno, e.strerror)
			return "Failed to create new model."
		except:
			print "Unexpected error:"
			return "Failed to create new model."
			raise

	def removemodel(self, username, modelname):
		try:
			if os.path.exists("usermodels/"+username):
				os.remove("usermodels/"+username+"/"+modelname+".npz");
				return modelname+" is removed."
			else:
				return modelname+" is not found."
		except:
			return "-"

	def removefromTraining(self, username, modelname, indexlist):
		try:
			npzfile = np.load("usermodels/"+username+"/"+modelname+".npz");
			indexlist = np.array(indexlist, dtype=bool);
			indexlist = np.insert(indexlist, 0, False);
			indexlist = np.invert(indexlist);
			x = npzfile['x']; 
			# print x.shape , indexlist
			x = x[indexlist];
			# print x.shape
			y = npzfile['y'];
			y = y[indexlist];
			filenames = npzfile['filename'];
			filenames = filenames[indexlist];
			starttimes = npzfile['start'];
			starttimes = starttimes[indexlist];
			stoptimes = npzfile['stop'];
			stoptimes = stoptimes[indexlist];

			np.savez("usermodels/"+username+"/"+modelname+".npz", x=x, y=y, filename=filenames, start=starttimes, stop=stoptimes)
			return "Remove operation successful."
		except:
			return "Cannot remove the file(s)!"
			print "Unexpected error:"
			raise


	def inspectmodel(self, username, modelname):
		try:
			npzfile = np.load("usermodels/"+username+"/"+modelname+".npz");
			y = npzfile['y'];
			filenames = npzfile['filename'];
			starttimes = npzfile['start'];
			stoptimes = npzfile['stop'];

			emolist = [];
			apnlist = [];
			pnnlist = [];

			# print y

			for i in range(1,y[:,0].size):
				# print np.array_str(filenames[i]), starttimes.size, stoptimes.size, filenames.size;
				emolist.append( np.array_str(filenames[i])[2:-2] +" " + emotionlist[y[i,0]-1] + " " + arousallist[y[i,1]-1] + " " + valancelist[y[i,2]-1]+ " " +np.array_str(starttimes[i])[1:-1]+ " " +np.array_str(stoptimes[i])[1:-1])


			# filestats = np.hstack(filenames, y);
			# filestats = np.hstack(filestats, starttimes);
			# filestats = np.hstack(filestats, stoptimes);

			# return json.dumps(emolist);
			return emolist;
		except IOError as e:
			print "I/O error({0}): {1}".format(e.errno, e.strerror)
		except:
			print "Unexpected error:"
			raise


	def addtoTraining(self, username, modelname, filename, emolabel, apnlabel, pnnlabel, start, stop):
		emotion = emotionlist.index(emolabel)+1;
		arousal = arousallist.index(apnlabel)+1;
		valance = valancelist.index(pnnlabel)+1;

		# print modelname;
		y = np.array([emotion,arousal,valance])

		start = float(start);
		stop = float(stop);

		filepath = '../public/uploads/'+username+'/'+filename;
		# print  emolabel, apnlabel, pnnlabel, start, stop, emotion, arousal, valance

		# vec = np.transpose(np.append(y, features))
		# print vec.shape

		try:
			
			npzfile = np.load("usermodels/"+username+"/"+modelname+".npz");
			
			features = self.extractFeatures(filepath, start, stop);
		
			# print features.shape, y.shape, filename.shape, start.shape, stop.shape

			x = np.vstack((npzfile['x'], features)) 
			datalength = npzfile['y'].shape[0];
			y = np.vstack((npzfile['y'], y))
			# print type(npzfile['filename']), npzfile['filename'].shape, datalength
			filenames = np.vstack((npzfile['filename'].reshape(datalength,1), filename));
			# print filenames
			starttimes = np.vstack((npzfile['start'], start));
			# print starttimes
			stoptimes = np.vstack((npzfile['stop'], stop));
			# print stoptimes

			

			np.savez("usermodels/"+username+"/"+modelname+".npz", x=x, y=y, filename=filenames, start=starttimes, stop=stoptimes)

		except IOError as e:
			features = self.extractFeatures(filepath, start, stop);
			np.savez("usermodels/"+username+"/"+modelname+".npz", x=features, y=y, filename=filename, start=start, stop=stop)
			# print "I/O error({0}): {1}".format(e.errno, e.strerror)
		except:
			return "Cannot add the file!"
			print "Unexpected error:"
			raise


		# self.createEmotionModel(username);
		# np.savez("usermodels/"+username+".npz", x=x, y=y);

		# npzfile = np.load("outfile.npz");
		self.printData(username, modelname);
		return filename + " (" + "{0:.2f}".format(start) + " - " + "{0:.2f}".format(stop) + ") has been added to the training data."
		# return '{0}, {1}, {2}, {3}, {4}, {5}, {6}'.format(username, filename, emolabel, apnlabel, pnnlabel, start, stop);

	def copyLDC(self, username):
		try:
			if not os.path.exists("usermodels/"+username):
				os.makedirs("usermodels/"+username)
			copyfile("LDC.npz", "usermodels/"+username+"/LDC.npz");
			return 1;
		except:
			return 0

	def printData(self, username, modelname):
		try:
			print "usermodels/"+username+"/"+modelname+".npz",
			npzfile = np.load("usermodels/"+username+"/"+modelname+".npz");
			print npzfile['x'].shape, npzfile['y'].shape, npzfile['filename'].shape, npzfile['start'].shape, npzfile['stop'].shape

			# self.createEmotionModel(username);
		except IOError as e:
			print "I/O error({0}): {1}".format(e.errno, e.strerror)
		except:
			print "Unexpected error:"
			raise

	def hello(self, name):
		return "Hello, %s" % name

# print extractFeatures('test.wav');

s = zerorpc.Server(emotionRPC())
s.bind("tcp://0.0.0.0:4242")
print "running";
s.run();

# To do: 
# accept button
